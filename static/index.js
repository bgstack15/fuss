/* Allow toggling the checkboxes with space, enter, and left/right */
let myLabels = document.querySelectorAll('.lbl-toggle');
Array.from(myLabels).forEach(label => {
  label.addEventListener('keydown', e => {
    // 32 spacebar, 13 enter
    if (e.which === 32 || e.which === 13) {
      e.preventDefault(); label.click();
    };
    // 37 left, 39 right
    if (e.which === 37 || e.which === 39) {
      var inputs = document.getElementsByTagName('INPUT');
      for (var i=0; i < inputs.length; i++) {
        if (inputs[i].id === e.target.htmlFor) {
          if (!inputs[i].checked && e.which === 39) {
            e.preventDefault(); label.click();
          }
          if (inputs[i].checked && e.which === 37) {
            e.preventDefault(); label.click();
          }
        }
      }
    }
  });
});

