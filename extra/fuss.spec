# File: fuss.spec
# Location: fuss package
# Author: bgstack15
# SPDX-License-Identifier: CC-BY-SA-4.0
# Startdate: 2021-01-05
# Title: Rpm spec for fuss package
# Purpose: Provide build instructions for CentOS rpm for package
# History:
# Usage:
# Reference:
# Improve:
# Documentation:
# Dependencies:

# Tunables
# If you set with_icons to 1, then you will pull in a large amount of dependencies!
%global with_icons 0
# If you set this to 1, use the systemd-rpm-macros functionality described at https://docs.fedoraproject.org/en-US/packaging-guidelines/UsersAndGroups/
%global with_systemd_usercreate 0
%global pythonver python36

# Fedora defaults
%if 0%{?fedora}
%global with_systemd_usercreate 1
%global with_icons 1
%global pythonver python3
%endif

%global _appvardir %{?_localstatedir}%{!?_localstatedir:/var}/www/fuss
%global _appdir %{?_datarootdir}%{!?_datarootdir:%{_prefix}/share}/fuss
%global _user fuss

%define devtty "/dev/null"
%define debug_package %{nil}
%global _python_bytecompile_errors_terminate_build 0

Summary:    file upload and storage service web app
Name:       fuss
Version:    0.0.2
Release:    1
License:    GPL 3.0
Source0:    %{name}_%{version}.orig.tar.gz
%if 0%{?with_systemd_usercreate}
Source1:    extra/%{name}.sysusers
%endif
%if ! 0%{?fedora}
Patch1:    extra/%{name}-el7.patch
%endif
URL:        https://bgstack15.wordpress.com/
#Distribution:
#Vendor:
Packager:   B. Stack <bgstack15@gmail.com>
Requires:   %{pythonver}-flask
Requires:   %{pythonver}-uwsgidecorators
Requires:   uwsgi-plugin-%{pythonver}
Requires:   uwsgi-logger-file
%if 0%{?fedora}
Requires:   %{pythonver}-magic
Requires:   %{pythonver}-flask-script
%endif
# Mandatory pip3 requirements: flask-script, python-magic
%if 0%{?with_icons}
Requires:   /usr/bin/xvfb-run
%if 0%{?fedora}
Requires:   %{pythonver}-gobject
%else
# The following are needed for CentOS7 pip3 install --user PyGObject
Requires:   cairo-gobject-devel
Requires:   gcc
Requires:   gobject-introspection-devel
Requires:   gtk3-devel
Requires:   python36-gobject
Requires:   python3-devel
%endif
%endif
#BuildRequires: txt2man
%if 0%{?with_systemd_usercreate}
BuildRequires: systemd-rpm-macros
%endif
%if 0%{?fedora} || 0%{?rhel} >= 8
Suggests: httpd
%endif
Buildarch:  noarch

%description
File Upload and Storage Service is a demo flask application that can
be used for whatever purposes you want.

%prep
%setup -q -c %{name}
test -d "%{name}" && cd "%{name}"
test -d "%{name}-master" && cd "%{name}-master"
%if ! 0%{?fedora}
%patch1 -p1
%endif

%build
export srcdir="extra"
test -d "%{name}" && cd "%{name}"
%make_build -C "${srcdir}"

%install
export srcdir="extra"
test -d "%{name}" && cd "%{name}"
%make_install -C "${srcdir}" \
   with_systemd=YES \
   with_init=NO \
   with_pip_helper=YES
%if 0%{?with_systemd_usercreate}
install -p -D -m 0644 %{SOURCE1} %{buildroot}%{_sysusersdir}/%{name}.conf
%endif
exit 0

%clean
rm -rf %{buildroot}

%pre
# Reference: squid.spec
%if 0%{?with_systemd_usercreate}
%sysusers_create_compat %{SOURCE1}
%else
if ! getent group %{_user} 1>/dev/null 2>&1 ;
then
   /usr/sbin/groupadd --system --gid 723 %{_user}
fi
if ! getent passwd %{_user} 1>/dev/null 2>&1 ;
then
   /usr/sbin/useradd --system --gid 723 \
      --uid 723 --comment "FUSS system user" \
      --home-dir %{_appvardir} --shell /sbin/nologin \
      %{_user}
fi
%endif
exit 0

%preun
%systemd_postun_with_restart %{name}.service

%post
%systemd_post %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%files
%if 0%{?with_systemd_usercreate}
%{_sysusersdir}/%{name}.conf
%endif
%attr(0644, %{_user}, %{_user}) %config(noreplace) %{_sysconfdir}/%{name}.conf
%attr(0644, %{_user}, %{_user}) %config(noreplace) %{_sysconfdir}/%{name}.wsgi.ini
%attr(0644, %{_user}, %{_user}) %{_libexecdir}/%{name}/%{name}.py
%attr(0755, %{_user}, %{_user}) %{_sbindir}/%{name}.bin
%attr(0644, %{_user}, %{_user}) %{_appdir}/static/*
%attr(0644, %{_user}, %{_user}) %{_appdir}/templates/*
%attr(0755, %{_user}, %{_user}) %dir %{_appdir}/static
%attr(0755, %{_user}, %{_user}) %dir %{_appdir}/templates
%attr(0755, %{_user}, %{_user}) %dir %{_appvardir}/upload
%attr(0755, %{_user}, %{_user}) %dir %{_appdir}
%attr(0755, %{_user}, %{_user}) %dir %{_appvardir}
%attr(0755, -, -) %{_libexecdir}/%{name}/pip-helper.sh
%attr(0644, root, root) %{?_unitdir}%{!?_unitdir:/usr/lib/systemd/system}/%{name}.service
%attr(0644, root, root) %{_sysconfdir}/httpd/conf.d/%{name}.conf
%attr(0755, %{_user}, %{_user}) %dir %{?_localstatedir}%{!?_localstatedir:/var}/log/%{name}
%{_defaultdocdir}/%{name}

%changelog
* Fri Sep 24 2021 B. Stack <bgstack15@gmail.com> - 0.0.2-1
- Upstream update

* Tue Jan 05 2021 B. Stack <bgstack15@gmail.com> - 0.0.1-1
- Initial release
