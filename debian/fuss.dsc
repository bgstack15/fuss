Format: 3.0 (quilt)
Source: fuss
Binary: fuss
Architecture: all
Version: 0.0.2-1
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://gitlab.com/bgstack15/fuss/
Standards-Version: 4.5.0
Build-Depends: debhelper-compat (= 12)
Files:
 00000000000000000000000000000000 1 fuss.orig.tar.gz
 00000000000000000000000000000000 1 fuss.debian.tar.xz
