# README for FUSS
FUSS is a File Upload and Storage Service.
Think [0x0](https://github.com/mia-0/0x0) but without the url shortening. This is intended for private use, such as behind password authentication on the web server level.

## Overview
FUSS runs a web front-end with a little landing page, and some summaries, and you can upload files to the web app. You can download files from it. This project is a demo of the original author's web app skills, and is designed to be relatively bare so you can modify or theme or improve it as you see fit.

## License
GPL 3.0

## How to use
Fuss is a wsgi application. You need some basic python3 packages, and then configure the two config files, which are self-explanatory.

* fuss.conf
* fuss.wsgi.ini

After configuring the files, you can run:

    /usr/sbin/fuss.bin

This short script runs uwsgi within xvfb-run, which is necessary if you want to use mimetype icons (loaded through Gtk). If you omit icons, you can omit the xvfb-run part.

You can run this with the included [init script](extras/fuss.init) if you prefer to have a system service.

### Running on certain distros
After installing the package, the admin might need to run a helper script that will install the necessary pip packages. Known situations include:
* CentOS 7: python-flask, python-magic

Run script:

    sudo /usr/libexec/fuss/pip-helper.sh

Add the parameter "PyGObject" if you compiled the rpm with icon support (which pulls in a lot of dependencies).

On Fedora, selinux prevents running /var/www/fuss/fuss.bin by systemd.

### Building a distro package
For an rpm, you can generate the correct type of tarball with:

    git archive master --format=tar.gz --prefix=fuss/ > ~/rpmbuild/SOURCES/fuss_0.0.2.orig.tar.gz

## Dependencies
* python3-flask
* python3-magic
* For icon support (optional), which just add mimetype icons to the html view of files.
  * Xvfb-run
  * PyGObject
  * python3-gi

## Alternatives
[0x0](https://github.com/mia-0/0x0) or my fork of it, [hex-zero](https://gitlab.com/bgstack15/hex-zero)

## References
### Overall
Heavily inspired by [0x0](https://github.com/mia-0/0x0) or my fork of it, [hex-zero](https://gitlab.com/bgstack15/hex-zero)
### Specific code snippets
* https://code-boxx.com/simple-drag-and-drop-file-upload/
* entire flask series at https://pythonise.com/series/learning-flask/flask-uploading-files
* hex_zero.py from https://gitlab.com/bgstack15/hex-zero
* https://stackoverflow.com/a/65433574/3569534
* https://werkzeug.palletsprojects.com/en/1.0.x/middleware/proxy_fix/
* https://uwsgi-docs.readthedocs.io/en/latest/Logging.html
* Reset stream when reading file from request: https://stackoverflow.com/a/26294982/3569534
* https://salsa.debian.org/sssd-team/sssd/-/blob/experimental/debian/sssd-common.postinst
* /home/var/lib/dpkg/info/postfix.prerm
* bgscripts.spec
* https://docs.fedoraproject.org/en-US/packaging-guidelines/UsersAndGroups/
* https://www.digitalocean.com/community/tutorials/css-collapsible
* viewport in html: https://stackoverflow.com/a/30709473/3569534
* https://www.thesitewizard.com/css/mobile-friendly-responsive-design.shtml
* https://www.dotnettricks.com/learn/stylesheet/css-to-force-long-text-and-urls-to-wrap-on-all-browser
* From https://stackoverflow.com/a/14226807/3569534
